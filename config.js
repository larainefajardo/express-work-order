module.exports = {
    SALT : "SaltoGameFarmDefaultSalt",
    JWT_key: "SaltoGameFarmDefaultSalt",
    page_limit: 10,
    password_history_length: 6,
    
    ALLOWED_DOMAINS : ['https://genericpwa.dice205.asia/','https://generic_api.dice205.asia/','http://localhost:2000/','http://localhost:3000/','http://localhost:3001/','https://dice205.com/','https://dice205.asia/'],
    SERVER_URL : "https://generic_api.dice205.asia",
    //SERVER_URL : "http://localhost:2000",
    SITE_URL : "http://localhost:3000",
    //name of your website
    WEBSITE_NAME: 'Generic Website',
    //original push notification ID
    SAFARI_WEB_PUSH_ID: 'web.dice205.generic.push',
    //generated public key. Run "npm run web-push generate-vapid-keys"
    WEB_PUSH_PUBLIC_KEY: 'BMsvaIgx1T_eiU-3JO4r34JW__FEatmVpXEj3N84lWqMvG9T4hQWdMAgIuhvMIJ9SU4LwsBl11iQ-bDS6wHBk34',
    //generated private key. From running "npm run web-push generate-vapid-keys"
    WEB_PUSH_PRIVATE_KEY: '_UBtUmMgF00cmKrAcikjhgDRP1Wyc0QX_LTFQt3CL7w',
    //developer team id. Get from membership section of developer.apple.com
    SAFARI_TEAM_ID : 'BT9JU4428Z',
    //from generate key. Get from developer.apple.com
    APN_KEY_ID : '88SR39P292',
    //just a unique string
    PUSH_AUTHENTICATION_TOKEN: 'generic_api.dice205.asia',
    //generate from developer.apple.com
    SAFARI_CERT : 'certs/website_aps_production.pem',
    //generate from keychain access
    SAFARI_PRIVATE_KEY : 'certs/cert_new.pem',
    //download from https://developer.apple.com/support/certificates/expiration
    SAFARI_INTERMEDIATE : 'certs/AppleWWDRCA.crt',
    //generated from developer.apple.com. This is the generated key. Just download it
    SAFARI_GENERATE_KEY: "certs/AuthKey_88SR39P292.p8",

    //Types of push notifications
    DEFAULT_PUSH_TYPE: 'default_push_type',
    SAFARI_PUSH_TYPE : 'safari_push_type',

    AWS_KEY: 'AKIAJGTBBVPZ4MJU674A',
    AWS_SECRET: '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',
    AWS_S3_BUCKET: 'dicegeneric',
    AWS_S3_UPLOADPATH: 'uploads',
    AWS_REGION: 'us-east-1',
    AWS_API_VERSION: '2006-03-01'

};
