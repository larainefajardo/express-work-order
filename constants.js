module.exports = {
		
	
	/*
	* Received Types
	*/
   
    HTTP_OK : 200,
    HTTP_CREATED : 201,
    HTTP_PARTIAL : 203,
    HTTP_ACCEOTED : 202,
    HTTP_REDIRECT : 300,
    
    HTTP_BAD_REQUEST : 400,
    HTTP_FORBIDDEN : 403,
    HTTP_UNAUTHORIZED : 401,
    HTTP_NOT_FOUND : 404,

    HTTP_INTERNAL_ERROR : 500,
    
    PERMISSION_GENERAL_READ : "general_read",
    PERMISSION_GENERAL_CREATE : "general_create",
    PERMISSION_GENERAL_UPDATE : "general_update",
    PERMISSION_GENERAL_DELETE : "general_delete",
    PERMISSION_GENERAL_REPORT : "general_report",

    PERMISSION_ADMIN_READ : "admin_read",
    PERMISSION_ADMIN_CREATE : "admin_create",
    PERMISSION_ADMIN_UPDATE : "admin_update",
    PERMISSION_ADMIN_DELETE : "admin_delete",
    PERMISSION_ADMIN_REPORT : "admin_report",

    THIRD_PARTY_FACEBOOK : "FB",
    THIRD_PARTY_GOOGLEPLUS : "GP",
    THIRD_PARTY_DEFAULT : "default",

};