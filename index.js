'use strict';


const express = require('express')
const mongoose = require('mongoose')
const crypto = require('crypto');
const cors = require('cors');
const bodyParser = require('body-parser');
const request = require('request');
const https = require('https');

const app = express();
var mutilpart = require('connect-multiparty');
var uploader = require('express-fileuploader');
var S3Strategy = require('express-fileuploader-s3');

const constants = require('./constants.js');
const router = require('./app/router.js'); 
const init = require('./app/init.js'); 

const port = process.env.PORT || 2000;
const database = process.env.DBSTRING ||'mongodb://127.0.0.1:27017/generic';
const environment = process.env.ENV || 'DEV';
const log_level = process.env.LOG_LEVEL || 'ALL';
const will_init = process.env.INIT || 'false';;

console.log(`willInit ... ` + will_init);

if(will_init !== 'false') {
	console.log(`initializing...`);
	init.init();
}else {
	console.log(`will not initialize...`);
}

console.log("connecting to " + database);
if(log_level === 'ERROR'){
	mongoose.set('debug', false);
}else {
	mongoose.set('debug', true);
}

mongoose.connect(database, { useNewUrlParser: true });
const db = mongoose.connection;

// app.use('/uploads', express.static('uploads'));
// app.use('/custom', express.static('custom'));
app.use(cors());
app.set('port', port);

//serve static files in the public directory
app.use(express.static('public'));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false, parameterLimit: 1000000}));

// let path = '/api/breederInventory/:gamefarm_id/upload';
// app.use(path, mutilpart());
 
// uploader.use(new S3Strategy({
//   uploadPath: 'uploads',
//   headers: {
//     'x-amz-acl': 'public-read'
//   },
//   options: {
//     key: 'AKIAJGTBBVPZ4MJU674A',
//     secret: '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',
//     bucket: 'saltoapp2'
//   }
// }));

// app.post(path, function(req, res, next) {
//   let gamefarm_id = req.params.gamefarm_id;		
//   uploader.upload('s3', req.files['images'], function(err, files) {
//     if (err) {
//       return next(err);
//     }
//     res.send(JSON.stringify(files));
//   });
// });


router.startRoutes(app);

app.listen(app.get('port'), function () {
	console.log('running on port', app.get('port'))
	
})



module.exports.app = app;