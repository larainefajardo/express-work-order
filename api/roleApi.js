'use strict';

const RestTemplate = require('./restTemplate.js');
const mongoose = require('mongoose');
const constants = require('../constants');
const config = require('../config');
const Debugger = require('../app/debugger.js'); 
//schemas to use
const Role = require('../schema/role');

const url = require('url');

class RoleApi extends RestTemplate{

    constructor(Request, Reply, Type = 'Role') {
		super(Request,Reply);
		this.Req = Request;
		this.Rep = Reply;
		this.Type = Type;
    }
    
    index() {
        const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let url_parts = url.parse(req.url, true);
        let query = url_parts.query;
        
        let page = 0;
        let limit = config.page_limit;
        let page_count = 1;
        if(query.page !== undefined || query.page !== null) {
            page = query.page - 1;
        }
        let offset_computation = config.page_limit * page;
        let jwtToken = req.headers['x-auth'];
        
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentRole = xAuth.user;
                Role.count({ deleted_at : null },function (err, count) {
                    page_count = count/limit;
                    page_count = Math.ceil(page_count);
                    Role.find({ deleted_at : null }, null , {sort: {role_id: -1}, skip: offset_computation, limit: limit},function (err, inventories) {
                        if (err) {
                            Debugger.logconsole(err,"ERROR");
                            res.setHeader('Access-Control-Allow-Origin','*');  
                            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                            res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                        }else {
                            res.setHeader('Access-Control-Allow-Origin','*');  
                            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                            res.status(constants.HTTP_OK).send({data: inventories,limit: limit, page_count: page_count, count: count, page : page + 1, offset: offset_computation});
                        }
                    });
                });
                
            }
        });
    }
    latest() {
        const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentRole = xAuth.user;
            
                Role.find().sort({role_id: -1}).limit(1).exec(function(err, inventories) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                    }else {
                        res.setHeader('Access-Control-Allow-Origin','*');            
                        res.status(constants.HTTP_OK).send(inventories);
                    }
                });
            }
        });
    }

    
   
    get(id) {
		const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentRole = xAuth.user;
                Role.findOne({$and : [ { role_id : id }, { deleted_at : null } ]  },function (err, role) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                    }else {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_OK).send(role);
                    }
                });
                
            }
        });
    }
    
    delete(id){
		const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_DELETE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentRole = xAuth.user;
                Role.findOneAndUpdate({$and: [ { 'role_id': id } ] }, {$set:{deleted_at: new Date()}}, { new: true, upsert: true }, function (err, role) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                    }else {
                        auditThis.logAudit(id, 'role', 'delete', JSON.stringify(role), null ,currentRole);
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.send({result: 'success', message: 'Deleted role'}).status(constants.HTTP_OK);
                    }
                    
                });
            }
        });
	}

    update(id) {
		const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_UPDATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentRole = xAuth.user;
                delete payload.role_id;
                
                Role.findOneAndUpdate({$and : [ { 'role_id' : id }, { deleted_at : null } ]  }, {$set:payload}, { returnOriginal:true,new: false, upsert: true }, function (err, role) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                
                    }else {
                        Debugger.logconsole(role);
                        let fromAudit = JSON.stringify(role);
                        let toAudit = JSON.stringify(payload);
                        if(fromAudit != toAudit)
                            auditThis.logAudit(id, 'role', 'update', fromAudit, toAudit,currentRole);
                        
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_OK).send({result: 'success', message: 'Updated role ' + id});
                    }
                    
                });
            }
        });
	}

    save() {
		const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let auditThis = this;
       
        Debugger.logconsole(payload);
        
        
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_CREATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentRole = xAuth.role;
                delete payload.role_id;
                
               
                let RoleObject = new Role(payload);
                RoleObject.save((err, roleObj) => {
                    if (err) {
                        //throw Boom.badRequest(err);
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                    }else {
                        auditThis.logAudit(roleObj.role_id, 'role', 'create', null, JSON.stringify(roleObj),currentRole);
                    
                        // If the role is saved successfully, issue a JWT
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.send({result: 'success', message: 'Saved role'}).status(constants.HTTP_OK);
                    }

                    
                });
            }
        });
    }


}

module.exports = RoleApi;