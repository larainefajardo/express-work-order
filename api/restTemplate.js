'use strict';

const boom = require('express-boom');
const Debugger = require('../app/debugger.js'); 
const url = require('url');
const config = require('../config');
const constant = require("../constants");
const jwt = require('jsonwebtoken');

class RestTemplate {
    
    constructor(Request, Reply, Type = 'Base') {
		this.request = Request;
		this.reply = Reply;
		this.Type = Type;
		
  	}

    index() {
        const req = this.Request;
		const res = this.Reply;
		let url_parts = url.parse(req.url, true);
		let query = url_parts.query;
		
		res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE'); 
        res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:"Get all function not implemented"});
	}
	
	latest() {
        const req = this.Req;
        const res = this.Rep;
		
		res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
        res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:"Latest function not implemented"});
	}

    get(id) {
		const req = this.Req;
        const res = this.Rep;
		

        res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');  
        res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:"Get function not implemented"});

    }
    
    save() {
		const req = this.Req;
        const res = this.Rep;
	    
		res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
		res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:'Create function not implemented'});
		
	}
	
	update(id) {
        const req = this.Req;
        const res = this.Rep;
		
		res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');  
		res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:'Update function not implemented'});	
		
	}

	search() {
		const req = this.Req;
        const res = this.Rep;
		res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE'); 
		res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:'Search not implemented'});
	}
	
	delete(id) {
		const req = this.Req;
        const res = this.Rep;
		res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
		res.status(constant.HTTP_NOT_FOUND).send({result: 'error', message:'Delete function not implemented'});
	}
	
	decodeJWT(jwt_token) {
		var decoded = jwt.verify(jwt_token, config.SALT);
		return decoded;
	}
	
	checkSecurity(xAuth, permission) {
		return new Promise(function(resolve, reject) { 
		
			if(xAuth == null || xAuth == undefined) {
				resolve({result: 'error', message: 'Unauthorized access', user: null})
			}
			try{
				
				
				var decoded = jwt.verify(xAuth, config.SALT);
				let roleName = "";

				
				if(decoded.role.name !== undefined) {
				
					Debugger.logconsole("decoded.role.name " + decoded.role.name);
					roleName = "ROLE_"+ decoded.role.name.toLowerCase();
				
				}else {
				
					Debugger.logconsole("decoded.role.Name " + decoded.role.name);
					roleName = "ROLE_"+ decoded.role.name.toLowerCase();
				
				}

				Debugger.logconsole("roleName " + roleName);
				// redis_client.get(roleName, function(err, permss) {
				// 	if(err) {
				// 		resolve ({result: 'error', message: 'Sorry I cannot recognize your jwt file', user: decoded})
				// 	}
					let permissions = decoded.role.permissions;
					if(permissions.indexOf(permission) > -1)
					{	
						resolve ({result: 'success', message: 'valid token', user: decoded})
					}else
					{
						resolve ({result: 'error', message: 'Sorry you do not have access to this permission', user: decoded})
					}

			// 	});
				
			}catch(err) {
				resolve ({result: 'error', message: err.message, user: null})
			}
		});
		
	}
	
	
	logAudit(id, apiName, action, from, to , creator_user) {
		
		console.log("audit creator_user ");
		console.log(creator_user);
		let created_by = creator_user.user_id;
		let creator_name = creator_user.full_name;

		let Changes = [];

		

        // let auditObject = new Audit({
			
		// 	ApiName : apiName,
		// 	Action : action,
		// 	From : from,
		// 	To : to,
		// 	created_by: created_by,
		// 	creator_name: creator_name
		// });
		
		// auditObject.save((err, auditResult) => {
		//     if (err) {
        //         //throw Boom.badRequest(err);
        //         Debugger.logconsole(err,"ERROR");
        //     }
        //     return true;
		// });
		return true;
	}
    
}

module.exports = RestTemplate;