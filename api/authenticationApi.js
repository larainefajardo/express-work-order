'use strict';

const RestTemplate = require('./restTemplate.js');
const mongoose = require('mongoose');
const constants = require('../constants');
const config = require('../config');
const Debugger = require('../app/debugger.js'); 
const {SHA256} = require('crypto-js');

const {sendEmail} = require('../app/emailer');

const APP_ID = ''; 
const jwt = require('jsonwebtoken');
//schemas to use
const utils = require('../app/utility.js'); 
const security = require('../app/security.js'); 
const url = require('url');

const User = require('../schema/user');
const Role = require('../schema/role');
const APP_URL_BASE = process.env.APP_URL_BASE || 'http://localhost:3000/';

class AuthenticationApi extends RestTemplate{

    constructor(Request, Reply, Type = 'Authentication') {
		super(Request,Reply);
		this.Req = Request;
		this.Rep = Reply;
        this.Type = Type;
    }
  
   
    login_no_client() {
        const req = this.Req;
        const res = this.Rep;
        
        let payload = req.body;
        payload.email = payload.email.toLowerCase();
        payload.password = SHA256(payload.password + config.SALT).toString();
        let referer = req.headers.referer; //  req.get('Referrer');
        let query = {$and : [{ email : payload.email },{ password : payload.password  },{ deleted_at : null }]};
        
        if(config.ALLOWED_DOMAINS.indexOf(referer) < 0) {
            
            res.setHeader('Access-Control-Allow-Origin','*');  
            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
            return res.send({result: 'error', message: 'Invalid Referrer Domain'}).status(constants.HTTP_FORBIDDEN);
        }

        if(payload.thirdparty_source !== null && payload.thirdparty_source !== undefined){
            if(payload.thirdparty_source === constants.THIRD_PARTY_FACEBOOK) {
                query = {$and : [{ email : payload.email },{ thirdparty_id : payload.thirdparty_id },{ deleted_at : null }]};
            } else {
                //THIRD_PARTY_DEFAULT
                query = {$and : [{ email : payload.email },{ password : payload.password  },{ deleted_at : null }]};
            }
        }

        
        User.findOne(query,function (err, user) {
           
            if(err) {
                Debugger.logconsole(err);
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.send({result: 'error', message: err.message}).status(201);
            }else {
                
                if(user == null || user == undefined) {
                    Debugger.logconsole("user here :");
                    Debugger.logconsole(user);
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.send({result: 'error', message: 'Invalid Credentials'}).status(201);
                }else {
                    
                    
                    //res.setHeader('x-auth',admins.tokens[0].token) ;
                    let userObj ={
                        full_name: user.full_name,
                        contact_number: user.contact_number,
                        position: user.position,
                        email: user.email,
                        id: user.user_id,
                        role: {}
                    }
                    let roleName = user.role.name;
                    
                    Role.findOne({$and : [ { name : roleName }, { deleted_at : null } ]  },function (err, role) {
                        if(role == null) {
                            res.setHeader('Access-Control-Allow-Origin','*');  
                            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                            res.send({result: 'error', message: 'No valid role assigned to this user'}).status(201);
                        }

                        var access = 'auth';
                        var token = jwt.sign({
                            _id: user._id.toHexString(), 
                            user_id: user.user_id, 
                            full_name:  user.full_name, 
                            email : user.email,
                            role: role,
                            access }, config.JWT_key).toString();

                            userObj.role = role;
                            
                            let now = new Date();

                            let checkPassword = security.passwordExpiry(role,user.password_history);
                            if(checkPassword === security.PASS) {
                                User.findOneAndUpdate({ 'user_id': userObj.id }, {$push:{last_login: now }}, { new: true, upsert: true }, function (err, admin) {
                                
                                    res.setHeader('Access-Control-Allow-Origin','*');  
                                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                                    res.send({result: 'success', message: token, user: userObj}).status(201);
                                });

                            }else {
                                res.setHeader('Access-Control-Allow-Origin','*');  
                                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                                res.send({result: 'success', message: checkPassword, user: []}).status(201);
                            }
                            
                            
                    });
                }
            }
                
        });
        
    }

    

    forgotPassword() { 
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        payload.email = payload.email.toLowerCase();
        payload.original_email = payload.email.toLowerCase();

        if(!req.body) return res.status(400).json({message: "No Request Body"});
        if(!req.body.email) return res.status(400).json({message: "No Email in Request Body"});
        
        const token = utils.randomString(40);
        const emailData = {
            to: req.body.email,
            subject: "Password Reset Instructions",
            text: `Please use the following link for instructions to reset your password: ${APP_URL_BASE}?token=${token}`,
            html: `<p>Please use the following link for instructions to reset your password: ${APP_URL_BASE}?token=${token}</p>`,
        };
    
        return User.update({email: payload.email}, {$set: {resetPassLink: token}}, function(error, feedback) {
            //NO PASSWORD EXPIRY YET.. PUT IN REDIS
            if(error) return res.send(error);
            else {
                sendEmail(emailData); 
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                return res.status(200).json({status: 'success',message: `Email has been sent to ${req.body.email}`});
            }
        });
    };
    
   
    resetpass() { 
        const req = this.Req;
        const res = this.Rep;

        const {resetPassLink, newPassword} = req.body;
        let password = SHA256(newPassword + config.SALT).toString();
        
        User.findOne({$and : [{ resetPassLink: resetPassLink },{ 'password_history.password' : password  },{ deleted_at : null }]},function (err, my_user) {
            if(my_user == null || my_user.length == 0) {
                User.findOne({$and : [{ resetPassLink: resetPassLink },{ deleted_at : null }]},function (err2, userObject) {
                    if(userObject == null) {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');  
                        return res.status(200).json({status: 'error', message: 'Invalid reset password token'});
                    }else {

                    
                        Role.findOne({'name': userObject.role.name },function (err3, roleObject) {
                            let checkPassword = security.passwordFormat(newPassword,password,roleObject,[]);
                            if(checkPassword === security.PASS){
                                User.update({resetPassLink: resetPassLink}, {$set: {resetPassLink: '', password: password, confirmed: true}, $push: { password_history : {password:password , last_update: new Date()}}}, function(error, feedback) {
                                    //NO PASSWORD EXPIRY YET.. PUT IN REDIS
                                    if(error) return res.send(error);
                                        res.setHeader('Access-Control-Allow-Origin','*');  
                                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                                    if(feedback.n > 0 && feedback.ok > 0) {
                                        return res.status(200).json({status: 'success', message: `Successfully reset password`});
                                    }else {
                                        return res.status(200).json({status: 'error', message: `Error in reset password, Token invalid`});
                                    }
                            
                                });
                            }else {
                                res.setHeader('Access-Control-Allow-Origin','*');  
                                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');  
                                return res.status(200).json({status: 'error', message: checkPassword});
                            }
                        });
                    }
                });
            }else {
                let password_history = my_user.password_history;
                let found = false;
                Debugger.logconsole("else");
                Debugger.logconsole(my_user);

                Role.findOne({'name': my_user.role.name },function (err3, roleObject) {
                    Debugger.logconsole("roleObject");
                    Debugger.logconsole(roleObject);
                    let checkPassword = security.passwordFormat(newPassword,password, roleObject,password_history);
                    if(checkPassword === security.PASS){
                    }else {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');  
                        return res.status(200).json({status: 'error', message: checkPassword});
                    }
                });
                
                console.log(password_history);
                if(config.password_history_length <= password_history.length) {
                    for(let i = 1; i <= config.password_history_length ; i++) {
                        let index = password_history.length - i;

                        if(index >= 0) {
                            
                            if(password_history[index].password == password) {
                                found = true;
                                res.setHeader('Access-Control-Allow-Origin','*');  
                                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                                return res.status(200).json({status: 'error', message: 'Sorry you cannot use your last '+config.password_history_length+' passwords for the change password'});
                            }
                        }
                        
                    }
                }else {
                    found = true;
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                    return res.status(200).json({status: 'error', message: 'Sorry you cannot use your last '+config.password_history_length+' passwords for the change password'});
                }
                
                if(!found) {
                    User.update({resetPassLink: resetPassLink}, {$set: {resetPassLink: '', password: password, confirmed: true}, $push: { password_history : {password:password , last_update: new Date()}}}, function(error, feedback) {
                        //NO PASSWORD EXPIRY YET.. PUT IN REDIS
                        if(error) return res.send(error);
                            res.setHeader('Access-Control-Allow-Origin','*');  
                            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
                        if(feedback.n > 0 && feedback.ok > 0) {
                                return res.status(200).json({status: 'success', message: `Successfully reset password`});
                        }else {
                                return res.status(200).json({status: 'error', message: `Error in reset password, Token invalid`});
                        }
                
                    });
                }
            }
        });
    
    };

    registerIfNotExists(registrant_profile) {
        return new Promise(function(resolve, reject) {
            registrant_profile.email = registrant_profile.email.toLowerCase();
            registrant_profile.original_email = registrant_profile.email.toLowerCase();
            registrant_profile.confirmed = false;  
            
            delete registrant_profile.user_id;
            if(registrant_profile.password != null && registrant_profile.password != undefined && registrant_profile.password != "") {
                registrant_profile.password = SHA256(registrant_profile.password + config.SALT).toString();
                registrant_profile.password_history = [];
                registrant_profile.password_history.push({password:registrant_profile.password , last_update: new Date()})  
            }

            if(registrant_profile.role == null || registrant_profile.role == undefined || registrant_profile.role == "") {
                registrant_profile.role = {
                    role_Id : 2,
                    permissions: [ "general_read", "general_create","general_update","general_delete","general_report"],
                    name : 'Subscriber'
                };
            }
            User.findOne({$and : [{ email: registrant_profile.email },{ deleted_at : null }]},function (err2, userobj) {
                if(userobj !== null && userobj !== undefined) {
                    reject('Email address is already token');
                }else {       
                    let UserObject = new User(registrant_profile);
                    UserObject.save((err, userObj) => {
                        if (err) {
                            //throw Boom.badRequest(err);
                            reject(err.message);
                        }else {
                            const emailData = {
                                to: req.body.email,
                                subject: "User Registration",
                                text: `Please use the following link for instructions to reset your password: ${APP_URL_BASE}?token=${token}`,
                                html: `<p>Please use the following link for instructions to reset your password: ${APP_URL_BASE}?token=${token}</p>`,
                            };
                            sendEmail(emailData); 
                            
                            resolve('Registered new user');
                        }
                    });
                }
            });
        });
    }

    register() { 
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let auditThis = this;
      
        Debugger.logconsole(payload);
        
        this.registerIfNotExists(payload)
        .then(function (message){
            res.setHeader('Access-Control-Allow-Origin','*');  
            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
            res.status(200).json({status: 'success', message: message});
                
        })
        .catch(function (message){
            res.setHeader('Access-Control-Allow-Origin','*');  
            res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');    
            res.status(constants.HTTP_BAD_REQUEST).json({status: 'error', message: message});
        })
        
    }
}

module.exports = AuthenticationApi;