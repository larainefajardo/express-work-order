'use strict';

const RestTemplate = require('./restTemplate.js');
const mongoose = require('mongoose');
const constants = require('../constants');
const config = require('../config');
const Debugger = require('../app/debugger.js'); 
const {SHA256} = require('crypto-js');

//schemas to use
const User = require('../schema/user');
const url = require('url');

class UserApi extends RestTemplate{

    constructor(Request, Reply, Type = 'User') {
		super(Request,Reply);
		this.Req = Request;
		this.Rep = Reply;
		this.Type = Type;
    }
    
    index() {
        const req = this.Req;
        const res = this.Rep;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                let user_id = currentUser.user_id;
                User.findOne({$and : [ { user_id :  user_id}, { deleted_at : null } ]  },'user_id full_name first_name last_name gender country city contact_number role.name email',function (err, user) {
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(constants.HTTP_OK).send(user);
                });
                
            }
        });
    }

    inbox() {
        const req = this.Req;
        const res = this.Rep;
        
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                let user_id = currentUser.user_id;
                User.findOne({$and : [ { user_id : user_id }, { deleted_at : null } ]  },'notification_list',function (err, user) {
                    if(err) {
                        res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: err.message});
                    }
                    console.log(user);
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(201).send(user.notification_list);
                });
                
            }
        });
    }

    delete(id){
		const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_DELETE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                let user_id = currentUser.user_id;
                User.findOneAndUpdate({$and: [ { 'user_id': user_id } ] }, {$set:{deleted_at: new Date()}}, { new: true, upsert: true }, function (err, user) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                    }else {
                        auditThis.logAudit(id, 'user', 'delete', JSON.stringify(user), null ,currentUser);
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.send({result: 'success', message: 'Your profile has been deleted'}).status(constants.HTTP_OK);
                    }
                    
                });
            }
        });
    }
   
    update(id) {
		const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        
        delete payload.email;
        delete payload.password;
        delete payload.user_id;
        delete payload.deleted_at;

        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_UPDATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                let user_id = currentUser.user_id;
                
                User.findOneAndUpdate({$and : [ { 'user_id' : user_id }, { deleted_at : null } ]  }, {$set:payload}, { returnOriginal:true,new: false, upsert: true }, function (err, user) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'error', message: err.message});
                
                    }else {
                        Debugger.logconsole(user);
                        let fromAudit = JSON.stringify(user);
                        let toAudit = JSON.stringify(payload);
                        if(fromAudit != toAudit)
                            auditThis.logAudit(user_id, 'user', 'update', fromAudit, toAudit,currentUser);
                        
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_OK).send({result: 'success', message: 'Successfully updated your profile'});
                    }
                    
                });
            }
        });
	}
    
    

}

module.exports = UserApi;