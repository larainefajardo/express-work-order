'use strict';

const RestTemplate = require('./restTemplate.js');
const webpusher = require('web-push');
const constants = require('../constants');
const Debugger = require('../app/debugger.js'); 
const {SHA256} = require('crypto-js');
const config = require('../config');
//schemas to use
const User = require('../schema/user');
const url = require('url');

class NotificationApi extends RestTemplate{

    constructor(Request, Reply, Type = 'User') {
		super(Request,Reply);
		this.Req = Request;
		this.Rep = Reply;
		this.Type = Type;
    }
    
    
    unregister(deviceToken, websitePushID){
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;

        console.log(websitePushID);
        console.log(deviceToken);
        console.log(payload);

        res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
        res.status(constants.HTTP_OK).send({result: 'success', message: 'OK'});
    }
    register(deviceToken, websitePushID){
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;

        console.log(websitePushID);
        console.log(deviceToken);
        console.log(payload);

        res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
        res.status(constants.HTTP_OK).send({result: 'success', message: 'OK'});
    }

    get_inbox({user_id} ) {
        const req = this.Req;
        const res = this.Rep;
        console.log(user_id);
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                User.findOne({$and : [ { user_id : user_id }, { deleted_at : null } ]  },'notification_list',function (err, user) {
                    if(err) {
                        res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: err.message});
                    }
                    console.log(user);
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(201).send(user.notification_list);
                });
                
            }
        });
    }
    
    my() {
        const req = this.Req;
        const res = this.Rep;
        
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                let user_id = currentUser.user_id;
                User.findOne({$and : [ { user_id : user_id }, { deleted_at : null } ]  },'notification_list',function (err, user) {
                    if(err) {
                        res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: err.message});
                    }
                    console.log(user);
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(201).send(user.notification_list);
                });
                
            }
        });
    }

    log(){
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;

        console.log(payload);

        res.setHeader('Access-Control-Allow-Origin','*');  
        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
        res.status(constants.HTTP_OK).send({result: 'success', message: 'OK'});
    }
    


    push_packages(websitePushID) {
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let MyConfig = config;
        var fs = require('fs');
        console.log("push_packages");
        if (!fs.existsSync(__dirname+ '/pushPackage.zip1')) {
            let pushLib = require('safari-push-notifications');
            var cert = fs.readFileSync(config.SAFARI_CERT),
            key = fs.readFileSync(config.SAFARI_PRIVATE_KEY),
            intermediate = fs.readFileSync(config.SAFARI_INTERMEDIATE),
             
            websiteJson = pushLib.websiteJSON(
                config.WEBSITE_NAME, // websiteName
                config.SAFARI_WEB_PUSH_ID, // websitePushID
                config.ALLOWED_DOMAINS, // allowedDomains
                config.SERVER_URL+'/%@/', // urlFormatString
                config.PUSH_AUTHENTICATION_TOKEN, // authenticationToken (zeroFilled to fit 16 chars)
                config.SERVER_URL // webServiceURL (Must be https!)
            );
            
            
            pushLib.generatePackage(
                websiteJson, // The object from before / your own website.json object
                'icons', // Folder containing the iconset
                cert, // Certificate
                key, // Private Key
                intermediate // Intermediate certificate
            )

            .pipe(fs.createWriteStream(__dirname+ '/pushPackage.zip'))
            .on('finish', function () {
                console.log('pushPackage.zip is ready.');
                //set the archive name
                res.sendFile(__dirname+ '/pushPackage.zip');
            });
        }else {
            res.sendFile(__dirname+ '/pushPackage.zip');
        }
        
    }
    push_sample() {
        var deviceToken = "998989C4F4BB5D89D63DCA82284D80C98E9B8DCD3925C81D41D44AD350FE8370";
        var apn = require('apn');
        var cert = config.SAFARI_CERT,
            key = config.SAFARI_PRIVATE_KEY;
        var teamId = config.SAFARI_TEAM_ID;
        var keyId = config.APN_KEY_ID;
        
        let service = new apn.Provider({
            cert: cert,
            key: key,
            production: true,
            token: {
                key: config.SAFARI_GENERATE_KEY,
                keyId: keyId,
                teamId: teamId
              },
        });
        let note = new apn.Notification();
            note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
            
            note.alert = "You have a new message";
            note.badge = 3;
            note.title = "Successful !!!"
            note.sound = "ping.aiff";
            note.action = "Test";
            note.body = "You have a new message";
            note.payload = {'messageFrom': 'John Appleseed'};
            note.urlArgs = ["test"];

            // The topic is usually the bundle identifier of your application.
            note.topic = "web.dice205.generic.push";
        
          console.log(`Sending: ${note.compile()} to ${deviceToken}`);
        
        service.send(note, deviceToken).then( result => {
            console.log("sent:", result.sent.length);
            console.log("failed:", result.failed.length);
            console.log(result.failed);
        });  

        service.shutdown();
    }

    send_message(user_id, title, message, sender_id) {
        return new Promise(function(resolve, reject) {
            User.findOne({$and : [ { user_id : user_id }, { deleted_at : null } ]  },function (err, user) {
                if (err) {
                    reject(err);
                }else {
                    if(user.webpush_tokens !== null&& user.webpush_tokens !== undefined) {
                        
                        //initialize web push notifications
                        webpusher.setVapidDetails('mailto:hello@dice205.com',config.WEB_PUSH_PUBLIC_KEY,config.WEB_PUSH_PRIVATE_KEY);
                        var apn = require('apn');
                        let service = new apn.Provider({
                            cert: config.SAFARI_CERT,
                            key: config.SAFARI_PRIVATE_KEY,
                            production: true,
                            token: {
                                key: config.SAFARI_GENERATE_KEY,
                                keyId: config.APN_KEY_ID,
                                teamId: config.SAFARI_TEAM_ID
                            },
                        });

                        let asyncLoop = require('node-async-loop');
                        asyncLoop(user.webpush_tokens, function (item, next){
                            console.log("---------------- Async loop item ------------------------");
                            console.log(item);
                            if(item === undefined || item === null) {
                                next();
                            }else if(item.type === config.DEFAULT_PUSH_TYPE) {
                                let webpush_options = JSON.parse(item.details);
                                let push_payload = JSON.stringify({title: title, content: message});
                                
                                console.log(webpush_options);
                                
                                webpusher.sendNotification(webpush_options,push_payload)
                                .then(function(e){
                                    next();
                                })
                                .catch(function(err) {
                                    console.log("Error: ");
                                    console.log(err);
                                    next();
                                    
                                })
                            }else if(item.type === config.SAFARI_PUSH_TYPE) {
                                var deviceToken = item.details;
                                let note = new apn.Notification();
                                    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
                                    
                                    note.alert = message;
                                    note.badge = 3;
                                    note.title = title
                                    note.sound = "ping.aiff";
                                    note.action = "Test";
                                    note.body = message;
                                    note.payload = {'messageFrom': 'John Appleseed'};
                                    note.urlArgs = ["test"];

                                    // The topic is usually the bundle identifier of your application.
                                    note.topic = config.SAFARI_WEB_PUSH_ID;
                                
                                console.log(`Sending: ${note.compile()} to ${deviceToken}`);
                                
                                service.send(note, deviceToken).then( result => {
                                    console.log("sent:", result.sent.length);
                                    console.log("failed:", result.failed.length);
                                    console.log(result.failed);
                                    next();
                                });  
                                
                            }
                            
                        }, function (err)
                        {
                            if (err)
                            {
                                console.error('Error: ' + err.message);
                                reject(err.message);
                                //return;
                            }
                            service.shutdown();
                            
                            let notification = {
                                message		:   message,	
                                title       :   title,
                                from        :   sender_id,
                                read        :   false,
                            }
                            User.findOneAndUpdate({$and: [ { 'user_id': user_id } ] }, {$push:{'notification_list' : notification}}, { new: true, upsert: true }, function (user_err, user) {
                                if(user_err) {
                                    console.error('Error: ' + user_err.message);  
                                    reject(user_err.message);
                                }
                                resolve('Sent all push notification');
                            });
                        });
                        
                        
                    }else {
                        reject('Sorry no subscription for push notification');
                    }
                }
            });
        });
    }

    send_notification() {
        
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let that = this;
        let message = payload.message;  
        let title = payload.title;
        let ids = payload.ids;

        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_CREATE).then((xAuth) => {
            
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                let sender_id = currentUser.user_id;
                let asyncLoop = require('node-async-loop');
                ids = ids.split(',');
                asyncLoop(ids, function (item, next){

                    that.send_message(item,title,message, sender_id)
                    .then(function(result){
                        next();
                    })
                    .catch(function(err) {
                        next();

                    });
                }, function (err)
                {
                    if (err)
                    {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(constants.HTTP_BAD_REQUEST).send({result: 'success', message: err.message});
                    }
                   

                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(constants.HTTP_OK).send({result: 'success', message: 'Successfully sent all notifications'});
                });    

            }
        });
    }
    send_notification_by_jwt() {
        
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let that = this;
        let message = payload.message;  
        let title = payload.title;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_CREATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                that.send_message(currentUser.user_id,title,message)
                .then(function(result){
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(constants.HTTP_OK).send({result: 'success', message: result});
                })
                .catch(function(err) {
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(constants.HTTP_INTERNAL_ERROR).send({result: 'error', message: err});

                });

            }
        });
    }

    store_vapid() {
        const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        console.log(payload);
        let webpush = {'type': payload.type,'details': payload.webpush};
        
        let jwtToken = req.headers['x-auth'];
        
        this.checkSecurity(jwtToken, constants.PERMISSION_GENERAL_CREATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                
                User.findOne({$and : [ { 'webpush_tokens.details' : payload.webpush }, { deleted_at : null } ]  },function (errorVapid, userWithVapid) {
                    if(errorVapid) {
                        console.log(errorVapid);
                    }
                    if(userWithVapid === null || userWithVapid === undefined) {
                        User.findOneAndUpdate({$and : [ { 'user_id' : currentUser.user_id }, { deleted_at : null } ]  }, {$push:{'webpush_tokens': webpush}}, { returnOriginal:true,new: false, upsert: true }, function (err, user) {
                            if (err) {
                                Debugger.logconsole(err,"ERROR");
                                res.setHeader('Access-Control-Allow-Origin','*');  
                                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                                res.status(201).send({result: 'error', message: err.message});
                        
                            }else {
                                Debugger.logconsole(user);
                                let fromAudit = JSON.stringify(user);
                                let toAudit = JSON.stringify(payload);
                                // if(fromAudit != toAudit)
                                //     auditThis.logAudit(id, 'user', 'update', fromAudit, toAudit,currentUser);
                                
                                res.setHeader('Access-Control-Allow-Origin','*');  
                                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                                res.status(201).send({result: 'success', message: 'Vapid successfully stored'});
                            }
                            
                        });
                    }else {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({result: 'error', message: 'Vapid already exists'});
                    }
                   

                });
            }
        });
    }

    


}

module.exports = NotificationApi;