'use strict';

const RestTemplate = require('./restTemplate.js');
const webpusher = require('web-push');
const constants = require('../constants');
const Debugger = require('../app/debugger.js'); 
const {SHA256} = require('crypto-js');
const config = require('../config');
const utility = require('../app/utility.js');
//schemas to use
const User = require('../schema/user');
const url = require('url');

class UserApi extends RestTemplate{

    constructor(Request, Reply, Type = 'User') {
		super(Request,Reply);
		this.Req = Request;
		this.Rep = Reply;
		this.Type = Type;
    }
    
    index() {
        const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let url_parts = url.parse(req.url, true);
        let query = url_parts.query;
        
        let page = 0;
        let limit = config.page_limit;
        let page_count = 1;
        if(query.page !== undefined || query.page !== null) {
            page = query.page - 1;
        }
        let offset_computation = config.page_limit * page;
        let jwtToken = req.headers['x-auth'];
        
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                User.count({ deleted_at : null },function (err, count) {
                    page_count = count/limit;
                    page_count = Math.ceil(page_count);
                    User.find({ deleted_at : null }, 'user_id full_name first_name last_name gender country city contact_number role.name email' , {sort: {user_id: -1}, skip: offset_computation, limit: limit},function (err, inventories) {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({data: inventories,limit: limit, page_count: page_count, count: count, page : page + 1, offset: offset_computation});
                    });
                });
                
            }
        });
    }

    all() {
        const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let url_parts = url.parse(req.url, true);
        let query = url_parts.query;
        
        let page = 0;
        let limit = config.page_limit;
        let page_count = 1;
        if(query.page !== undefined || query.page !== null) {
            page = query.page - 1;
        }
        let offset_computation = config.page_limit * page;
        let jwtToken = req.headers['x-auth'];
        
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                User.count({ deleted_at : null },function (err, count) {
                    page_count = count/limit;
                    page_count = Math.ceil(page_count);
                    User.find({ deleted_at : null }, 'user_id full_name first_name last_name gender country city contact_number role.name email' , {sort: {user_id: -1}},function (err, users) {
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({data: users});
                    });
                });
                
            }
        });
    }

    search() {
        const req = this.Req;
        const res = this.Rep;
        const payload = req.body;

        let jwtToken = req.headers['x-auth'];

        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let qName = payload.qname;
            
                User.find({$and : [{$or : [ { full_name :  { '$regex' : qName, '$options' : 'i' } },{ first_name : { '$regex' : qName, '$options' : 'i' }} , { last_name : { '$regex' : qName, '$options' : 'i' } }, { email : { '$regex' : qName, '$options' : 'i' } }  ]}, { deleted_at : null } ] },'user_id full_name first_name last_name gender country city contact_number role.name email',).sort({user_id: -1}).exec(function(err, search_results) {
                    res.setHeader('Access-Control-Allow-Origin','*');            
                    res.status(201).send(search_results);
                });
            }
        });
    }

    latest() {
        const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        
        let jwtToken = req.headers['x-auth'];

        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
            
                User.find({},'user_id full_name first_name last_name gender country city contact_number role.name email',).sort({user_id: -1}).limit(1).exec(function(err, inventories) {
                    res.setHeader('Access-Control-Allow-Origin','*');            
                    res.status(201).send(inventories);
                });
            }
        });
    }
    
    get(id) {
		const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_READ).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                User.findOne({$and : [ { user_id : id }, { deleted_at : null } ]  },'user_id email full_name first_name last_name gender country city contact_number role resetPassLink',function (err, user) {
                    if(err) {
                        res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: err.message});
                    }
                    console.log(user);
                    res.setHeader('Access-Control-Allow-Origin','*');  
                    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                    res.status(201).send(user);
                });
                
            }
        });
    }
    
    delete(id){
		const req = this.Req;
        const res = this.Rep;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_DELETE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                User.findOneAndUpdate({$and: [ { 'user_id': id } ] }, {$set:{deleted_at: new Date()}}, { new: true, upsert: true }, function (err, user) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({result: 'error', message: err.message});
                    }else {
                        auditThis.logAudit(id, 'user', 'delete', JSON.stringify(user), null ,currentUser);
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.send({result: 'success', message: 'Deleted user'}).status(201);
                    }
                    
                });
            }
        });
    }
   
    update(id) {
		const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let auditThis = this;
        let jwtToken = req.headers['x-auth'];
        
        delete payload.email;
        delete payload.confirm_email;
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_UPDATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                delete payload.user_id;
                if(payload.password != null && payload.password != undefined && payload.password != "") {
                    payload.password = SHA256(payload.password + config.SALT).toString();
                }

                User.findOneAndUpdate({$and : [ { 'user_id' : id }, { deleted_at : null } ]  }, {$set:payload}, { returnOriginal:true,new: false, upsert: true }, function (err, user) {
                    if (err) {
                        Debugger.logconsole(err,"ERROR");
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({result: 'error', message: err.message});
                
                    }else {
                        Debugger.logconsole(user);
                        let fromAudit = JSON.stringify(user);
                        let toAudit = JSON.stringify(payload);
                        if(fromAudit != toAudit)
                            auditThis.logAudit(id, 'user', 'update', fromAudit, toAudit,currentUser);
                        
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({result: 'success', message: 'Updated user ' + id});
                    }
                    
                });
            }
        });
	}
    
    save() {
		const req = this.Req;
        const res = this.Rep;
        let payload = req.body;
        let auditThis = this;

        payload.email = payload.email.toLowerCase();
        payload.original_email = payload.email.toLowerCase();

      
        let jwtToken = req.headers['x-auth'];
        this.checkSecurity(jwtToken, constants.PERMISSION_ADMIN_CREATE).then((xAuth) => {
        
            if(xAuth.result != 'success') {
                res.setHeader('Access-Control-Allow-Origin','*');  
                res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                res.status(constants.HTTP_FORBIDDEN).send({result: 'error', message: xAuth.message});
            }
            else {               
                let currentUser = xAuth.user;
                delete payload.user_id;
                if(payload.password != null && payload.password != undefined && payload.password != "") {
                    payload.password = SHA256(payload.password + config.SALT).toString();
                    payload.confirmed = true;
                }
                
                if(payload.role == null || payload.role == undefined || payload.role == "") {
                    payload.role = {
                        role_Id : 2,
                        permissions: [ "general_read", "general_create","general_update","general_delete","general_report"],
                         name : 'Subscriber'
                    };
                }
                
                if(utility.jsonCheck(payload.role)) {
                    payload.role = JSON.parse(payload.role);
                }

                let UserObject = new User(payload);
                UserObject.save((err, userObj) => {
                    if (err) {
                        //throw Boom.badRequest(err);
                      
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.status(201).send({result: 'error', message: err.message});
                    }else {
                        auditThis.logAudit(userObj.user_id, 'user', 'create', null, JSON.stringify(userObj),currentUser);
                        
                        // If the user is saved successfully, issue a JWT
                        res.setHeader('Access-Control-Allow-Origin','*');  
                        res.setHeader('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE');
                        res.send({result: 'success', message: 'Saved user'}).status(201);
                    }

                    
                });
            }
        });
    }


}

module.exports = UserApi;