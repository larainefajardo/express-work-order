const security = require('../app/security.js'); 
const config = require('../config');
const {SHA256} = require('crypto-js');
const expect = require('expect');

it("passwordExpiry: test that a password will expire", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 16,
        max_password_age : 1,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let password_history = [
        {last_update: new Date('2017-10-01 13:29:19.283')}
    ]
    var res = security.passwordExpiry(Role,password_history);
    expect(res.toString()).toBe(security.FAIL_PASSWORD_EXPIRED);
});

it("passwordExpiry: test that a password will NOT expire", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 6,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let password_history = [
        {last_update: new Date('2018-10-02 13:29:19.283')}
    ]
    var res = security.passwordExpiry(Role,password_history);
    expect(res.toString()).toBe(security.PASS);
});

it("passwordFormat: test that a password will pass", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 6,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'P@ssw0rd123';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = [];
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.PASS);
});


it("passwordFormat: test that a password will fail on minimum characters", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 16,
        max_password_age : 40000,
        min_password_length : 18,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'P@ssw0rd123';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = [
        {password: 'sampletest' , last_update: new Date('2018-10-02 13:29:19.283')}
    ]
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.FAIL_MINIMUM_LENGTH);
});

it("passwordFormat: test that a password will fail on special characters", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 16,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'Passw0rd123';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = [
        {password: 'sampletest' , last_update: new Date('2018-10-02 13:29:19.283')}
    ]
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.FAIL_SPECIAL_CHARACTERS);
});

it("passwordFormat: test that a password will fail on alphanumeric characters", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 16,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        has_number : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'P@sswprdaaaa';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = [
        {password: 'sampletest' , last_update: new Date('2018-10-02 13:29:19.283')}
    ]
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.FAIL_ALPHA_NUMERIC);
});

it("passwordFormat: test that a password will fail on last password used characters", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 2,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        has_number : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'P@sswprd123';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = []
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.PASS);
});

it("passwordFormat: test that a password will fail on last password used characters", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 3,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        has_number : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'P@sswprd123';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = [
        {password: hashed_password+'123' , last_update: new Date('2018-10-02 13:29:19.283')},
        {password: hashed_password+'123' , last_update: new Date('2018-10-02 13:29:19.283')},
        {password: hashed_password , last_update: new Date('2018-10-02 13:29:19.283')}
    ]
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.FAIL_LENGTH);
});

it("passwordFormat: test that a password will PASS on last password used characters", () => {
    let Role = {
        role_id : 3,
        permissions: [],
        password_history_length : 2,
        max_password_age : 40000,
        min_password_length : 8,
        has_special_characters : true,
        has_uppercase_characters : true,
        has_lowercase_characters : true,
        has_number : true,
        created_by : 1,
        created_at : new Date('2018-10-02 12:33:56.415'),
        deleted_by :0,
        deleted_at : null,
        name : "Subscriber"
    };
    let = password = 'P@sswprd123';
    let hashed_password = SHA256(password + config.SALT).toString();
    let password_history = [
        {password: hashed_password , last_update: new Date('2018-10-02 13:29:19.283')},
        {password: hashed_password+'123' , last_update: new Date('2018-10-02 13:29:19.283')},
        {password: hashed_password+'1234' , last_update: new Date('2018-10-02 13:29:19.283')},
      
    ]
    //passwordFormat = (password, hashed_password, Role, password_history)
    var res = security.passwordFormat(password, hashed_password, Role, password_history);
    expect(res.toString()).toBe(security.PASS);
});