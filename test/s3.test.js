const s3 = require('../app/s3.js'); 
const expect = require('expect');


it("Create Bucket", () => {
    
    s3.createBucket().then(function(res) {
        expect(res).toBe(true);
    });
    
});

it("getObject Bucket", () => {
    
    res = s3.getObject('icons8-notification-center-50.png');
    expect(res).toBeA('String');
    
    
});
