const utility = require('../app/utility.js'); 
const config = require('../config');
const expect = require('expect');

it("addDate: should be able to add days", () => {
    var res = utility.addDate(new Date('01/12/2018'), 10);
    expect(res.toString()).toBe(new Date('01/22/2018').toString());
});

it("addDate: should be able to subtract days given a negative number", () => {
    var res = utility.addDate(new Date('01/12/2018'), -10);
    expect(res.toString()).toBe(new Date('01/02/2018').toString());
});

it("subtractDate: should be able to add days", () => {
    var res = utility.subtractDate(new Date('01/12/2018'), 10);
    expect(res.toString()).toBe(new Date('01/02/2018').toString());
});

it("subtractDate: should be able to subtract days", () => {
    var res = utility.subtractDate(new Date('01/12/2018'), -10);
    expect(res.toString()).toBe(new Date('01/22/2018').toString());
});

it("prettifyDate: should be able to show prettified text - January 12, 2018", () => {
    var res = utility.prettifyDate(new Date('01/12/2018'));
    expect(res).toBe('January 12, 2018');
});

it("datediff: should be able to show how many days in between of both dates", () => {
    var res = utility.datediff(new Date('01/12/2018'), new Date('01/22/2018'));
    expect(res).toBe(10);
});

it("jsonCheck: check if string is a JSON object, should be true", () => {
    let forChecking = [{
        name: 'dice205',
        app: 'salto app2'
    }]
    var res = utility.jsonCheck(JSON.stringify(forChecking));
    expect(res).toBe(true);
});

it("jsonCheck: check if string is a JSON object, should be false", () => {
    let forChecking = [{
        name: 'dice205',
        app: 'salto app2'
    }]
    var res = utility.jsonCheck(forChecking);
    expect(res).toBe(false);
});

it("arraySort: check sorting of arrays by property", () => {
    let forChecking = [
        {
            name: 'golf',
        },
        {
            name: 'charlie',
        },
        {
            name: 'beta',
        },
        {
            name: 'delta',
        },
        {
            name: 'alpha',
        }
    ];

    let correctAnswer = [{
            name: 'alpha',
        },
        {
            name: 'beta',
        },
        {
            name: 'charlie',
        },
        {
            name: 'delta',
        },
        {
            name: 'golf',
        }
    ];
    var res = utility.arraySort(forChecking, 'name');
    expect(JSON.stringify(res)).toEqual(JSON.stringify(correctAnswer));
});

it("randomString: check if a random string will be generated", () => {
    let forChecking = [{
        name: 'dice205',
        app: 'salto app2'
    }]
    var res = utility.randomString(10);
    expect(res.length).toEqual(10);
});


it('test zeroFill : basic', () => {
   expect(utility.zeroFill(1, 3)).toBe("001");
});
  
it('test zeroFill : no zeros to fill', () => {
    var number = 123
    var result = utility.zeroFill(number, 3);
  
    expect(typeof result).toBe('string');
    expect(result).toBe(String(number));
});
  
it('test zeroFill : decimal point isn\'t considered', () => {
    var number = 456.7
    var result = utility.zeroFill(number, 6);
  
    expect(typeof result).toBe('string');
    expect(result).toBe('00456.7');
});
  
it('test zeroFill : test sha512', () => {
    expect(utility.sha512('itworks')).toEqual({"hashType": "sha512", "hashValue": "ff3470c61d4e3f66e69bf472fd48ff67b42be150625aa0d825f0fa86dd7cb6c01e54f0c810b9208371ae3295cf892ae051e93455a041dcdafcf2cffb5d2a245a"});
});
