'use strict';

const config = require('../config');
const log_level = process.env.LOG_LEVEL || config.LOG_LEVEL;

let logconsole = (message, level ) => {
    let willLog = false;
    

    let logs_map = new Map();
    
    logs_map.set('ERROR', 1);
    logs_map.set('DEBUG', 2);
    logs_map.set('WARN', 3);
    logs_map.set('INFO', 4);
    logs_map.set('ALL', 5);
    
    
    if(level == undefined || level == null)
    {
        level = "DEBUG";
    }
    
    if(log_level === "NONE") {
        willLog = false;
    }
    else if(logs_map.get(log_level) >= logs_map.get(level)){
        willLog = true;
    }

   // if(willLog)
        console.log(message);
    
    let test = logs_map.get(level);
    message = "test";
   
    return test + ":" + message;
}

module.exports = { 
    logconsole: logconsole
};