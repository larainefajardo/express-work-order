const PASS = "Valid Password Format";
const FAIL_LENGTH = "Password should not be less than minimum last passwords";
const FAIL_SPECIAL_CHARACTERS = "Password should have special characters";
const FAIL_MINIMUM_LENGTH = "Password length is less than minimum allowed length";
const FAIL_UPPER_LOWER_CASE = "Password should have an upper and lower case";
const FAIL_ALPHA_NUMERIC = "Password should be alpha numeric";
const FAIL_PASSWORD_EXPIRED = "You should change this password already";
const utility = require('./utility');

module.exports = {
    PASS : PASS,
    FAIL_LENGTH : FAIL_LENGTH,
    FAIL_SPECIAL_CHARACTERS : FAIL_SPECIAL_CHARACTERS,
    FAIL_MINIMUM_LENGTH : FAIL_MINIMUM_LENGTH,
    FAIL_UPPER_LOWER_CASE : FAIL_UPPER_LOWER_CASE,
    FAIL_ALPHA_NUMERIC : FAIL_ALPHA_NUMERIC,
    FAIL_PASSWORD_EXPIRED : FAIL_PASSWORD_EXPIRED,
};
module.exports.passwordExpiry = (Role, password_history) => {

    if(Role.max_password_age <= 0) {
        return PASS;
    }else {
        let index = password_history.length - 1;
        let diff = 0
        
        if(index > 0) {
            diff = utility.datediff(password_history[index].last_update,Date.now());
        }
       
        if(diff >= Role.max_password_age) {
            return FAIL_PASSWORD_EXPIRED;
        }else {
            return PASS;
        }

    }
    

}

module.exports.passwordFormat = (password, hashed_password, Role, password_history) => {
    var code, i, len;
    var foundNumbers = false;
    var foundCharacters = false;
    var foundSpecialChar = false;
  
    
    if(password.length < Role.min_password_length){
        return FAIL_MINIMUM_LENGTH;
    }

    for (i = 0, len = password.length; i < len; i++) {
      code = password.charCodeAt(i);
      if ((code > 64 && code < 91) || // upper alpha (A-Z)
          (code > 96 && code < 123)) { // lower alpha (a-z)
        foundCharacters = true;
      }else if (code > 47 && code < 58) { // numeric (0-9) 
        foundNumbers = true;
      }else if((code > 32 && code < 48) || 
        (code > 59 && code < 65) ||
        (code > 90 && code < 97) ||
        (code > 122 && code < 127) ) {
            foundSpecialChar = true;
      }
    }
    
    if(Role.has_uppercase_characters) {
        if(!foundCharacters) {
            return FAIL_UPPER_LOWER_CASE;
        }
    }

    if(Role.has_number) {
        if(!foundNumbers) {
            return FAIL_ALPHA_NUMERIC;
        }
    }

    if(Role.has_special_characters) {
        if(!foundSpecialChar) {
            return FAIL_SPECIAL_CHARACTERS;
        }
    }

    let found = false;
    
    if(password_history.length == 0) {
        return PASS;
    }
    if(Role.password_history_length <= password_history.length) {
        for(let i = 1; i <= Role.password_history_length ; i++) {
            let index = password_history.length - i;
            
            if(index >= 0) {
                if(password_history[index].password === hashed_password) {
                    found = true;
                    return FAIL_LENGTH;
                }
            }
            
        }
    }else {
        found = true;
        return FAIL_LENGTH;
    }

    if(!found) {
        return PASS;
    }

  }