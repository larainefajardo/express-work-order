module.exports.zeroFill = (number, width) => {
    width -= number.toString().length;
    if (width > 0) {
        return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + ""; // always return a string
}

module.exports.sha512 = (content) => {
    var crypto = require('crypto');
    var shasum = crypto.createHash('sha512');
    
    shasum.update(content);
    return {
        "hashType": "sha512",
        "hashValue": shasum.digest('hex')
    };
}

module.exports.addDate = (targetDate, numberOfDays) => {
    let newdate = new Date(targetDate);
    newdate.setDate(newdate.getDate() + numberOfDays);
    return newdate;
}

module.exports.subtractDate = (targetDate, numberOfDays) => {
    let newdate = new Date(targetDate);
    newdate.setDate(newdate.getDate() - numberOfDays);
    return newdate;
}

module.exports.prettifyDate = (my_date) => {
    let date = new Date(my_date);
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return  months[date.getMonth()]  + ' ' + date.getDate() + ', ' + date.getFullYear();
}
module.exports.randomString = length => {
    let text = "";
    const possible = "abcdefghijklmnopqrstuvwxyz0123456789_-.";
    for(let i =0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    }
    return text;

}

module.exports.arraySort = (arrayOfObjs, property_name) => {
    function compare(a,b) {
        if (a[property_name] < b[property_name])
          return -1;
        if (a[property_name] > b[property_name])
          return 1;
        return 0;
    }
    let sortedArray = arrayOfObjs.sort(compare);
    return sortedArray;
}

module.exports.jsonCheck = (testString) => {
    try
    {
      var json = JSON.parse(testString);
      return true;
    }
    catch(e)
    {
      return false;
    }
}

module.exports.parseDate = (str)  => {
    var mdy = str.split('/');
    return new Date(mdy[0], mdy[0], mdy[1]);
}

module.exports.datediff = (first, second)  => {
    // first = new Date(first);
    // second = new Date(second);
    
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second-first)/(1000*60*60*24));
}
