'use strict';

const mongoose = require('mongoose');
const config = require('../config');
const constants = require('../constants');
const {SHA256} = require('crypto-js');

let defaultPassword = SHA256('P@ssw0rd123' + config.SALT).toString();
const database = process.env.DBSTRING ||'mongodb://127.0.0.1:27017/generic';
const environment = process.env.ENV || 'DEV';
const log_level = process.env.LOG_LEVEL || 'ALL';


var initialize = function(){
    
    mongoose.connect(database, { useNewUrlParser: true });
    const db = mongoose.connection;

    var data = [
    {
        'model': 'user',
        'identifier': 'email',
        'documents': [
            {

                email : "aldrich.abrogena@dice205.com",
                original_email : "aldrich.abrogena@dice205.com",
                password : defaultPassword,
                first_name: "Alvin Aldrich",
                last_name: "Abrogena",
                
                gender: "M",
                country: "PH",
                city: "Makati",
                product_line: "SALTO",
                full_name : "Alvin Aldrich Abrogena",
                contact_number : "09178546316",
                role: {
                    role_Id : 1,
                    permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                                   "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                    name : 'Super Admin',
                },
                created_at : new Date(),
                created_by: 1,
	            deleted_by : 0,
                deleted_at : null,

                
            },
            {

                email : "laraine.fajardo@dice205.com",
                original_email : "laraine.fajardo@dice205.com",
                first_name: "Laraine",
                last_name: "Fajardo",
                
                gender: "F",
                country: "PH",
                city: "Makati",
                product_line: "SALTO",

                password : defaultPassword,
                full_name : "Laraine Fajardo",
                contact_number : "09178546316",
                role: {
                    role_Id : 1,
                    permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                                   "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                    name : 'Super Admin',
                },
                created_at : new Date(),
                created_by: 1,
	            deleted_by : 0,
                deleted_at : null,

                
            },
            {

                email : "geraldine.dy@dice205.com",
                original_email : "geraldine.dy@dice205.com",
                first_name: "Geraldine",
                last_name: "Dy",
                
                gender: "F",
                country: "PH",
                city: "Mandaluyong",
                product_line: "SALTO",
                password : defaultPassword,
                full_name : "Geraldine Dy",
                contact_number : "09178546316",
                role: {
                    role_Id : 1,
                    permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                                   "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                    name : 'Super Admin',
                },
                created_at : new Date(),
                created_by: 1,
	            deleted_by : 0,
                deleted_at : null,

                
            },
            {
                email : "raphael.estrada@dice205.com ",
                original_email : "raphael.estrada@dice205.com ",
                first_name: "Raphael",
                last_name: "Estrada",
                
                gender: "M",
                country: "PH",
                city: "Makati",
               
                password : defaultPassword,
                full_name : "Raphael Estrada",
                contact_number : "0917111111",
                role: {
                    role_Id : 1,
                    permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                                   "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                    name : 'Super Admin',
                },
                created_at : new Date(),
                created_by: 1,
                deleted_by : 0,
                deleted_at : null,
             },
             {
                email : "glenn.salvosa@dice205.com",
                original_email : "glenn.salvosa@dice205.com",
                first_name: "Glenn",
                last_name: "Salvosa",
                
                gender: "M",
                country: "PH",
                city: "Makati",
               
                password : defaultPassword,
                full_name : "Glenn Salvosa",
                contact_number : "09178882211",
                role: {
                    role_Id : 1,
                    permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                                   "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                    name : 'Super Admin',
                },
                created_at : new Date(),
                created_by: 1,
                deleted_by : 0,
                deleted_at : null,
             }
        ]
    },
    {
        'model': 'role',
        'identifier': 'name',
        'documents': [
            {
                permissions: [ "general_read", "general_create","general_update","general_delete","general_report",
                                   "admin_read","admin_create","admin_update","admin_delete","admin_report"],
                
                name : 'Super Admin',
                password_history_length : 6,
                max_password_age: 180,
                min_password_length: 8,
                has_special_characters : true,
                has_uppercase_characters :true,
                has_lowercase_characters : true,
                has_number: true,

                created_at : new Date(),
                created_by: 1,
	            deleted_by : 0,
                deleted_at : null,
            },
            {
                permissions: [ "general_read", "general_create","general_update","general_delete","general_report"],
                name : 'Subscriber',
                password_history_length : 6,
                max_password_age: 0,
                min_password_length: 8,
                has_special_characters : true,
                has_uppercase_characters :true,
                has_lowercase_characters : true,
                has_number: true,

                created_at : new Date(),
                created_by: 1,
	            deleted_by : 0,
                deleted_at : null,
            },
            {
                permissions: [ "general_read", "general_report"],
                name : 'Report',

                password_history_length : 6,
                max_password_age: 0,
                min_password_length: 8,
                has_special_characters : true,
                has_uppercase_characters :true,
                has_lowercase_characters : true,
                has_number: true,

                created_at : new Date(),
                created_by: 1,
	            deleted_by : 0,
                deleted_at : null,
            },

        ]
    }
    ];

    if(data !== null && data !== undefined && data.length > 0) {
        for(let i = 0; i < data.length; i++) {
            let objectPath = '../schema/'+ data[i].model+'.js';
            let className = require(objectPath);
            
            for(let j = 0; j < data[i].documents.length; j++) {
                let document = data[i].documents[j];
                let identifier = data[i].identifier;
                
                
                var condition = {};
                condition[identifier] = document[identifier];
               
                try {
                    className['findOne'](condition,function (err,result) {
                        if(err) {
                            console.log('Error');
                            console.log(err); 
                        }
                        else {
                           
                            if(!result) {
                                let myObject = new className(document);
                                myObject['save'](function(err2, result2){
                                    console.log('Added New ' + data[i].model);
                                    console.log(result2);
                                });
                            }else {
                                className['findOneAndUpdate'](condition, {$set:document}, { new: true, upsert: true }, function (err2, result2) {
                                    console.log('Success Update' + data[i].model);
                                    console.log(result2);
                                });
                            }
                        
                        }
                    });
                }catch(except) {
                    console.log('error catch');
                    console.log(except);
                }
                
                
            }
            
        }
    }
    
}

module.exports = {
    init: initialize 
};