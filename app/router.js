
'use strict';

const config = require('../config');
const constants = require('../constants');
const Debugger = require('../app/debugger.js'); 
const jwt = require('jsonwebtoken');
const init = require('./init.js'); 

let myapp = null;

/* for static return strings from url only */

module.exports.add_static_path = (path, messageText) => {
	Debugger.logconsole("GET  :" + path);
    this.myapp.get(path, function (req, res) {
        res.send(messageText);
    })

}

/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page. 
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
module.exports.add_post_path = (api,methodName) => {
    let apiController = ''

    let objectPath = '../api/'+api+'Api.js' 
	let className = require(objectPath);
	let titlePath = api; //api.toLowerCase();
    let controllerName = api+'Api';
    let path = '/api/'+ titlePath+'/'+methodName;
    
    Debugger.logconsole("POST :" + path);

    this.myapp.post(path,function (req, res) {
        let myObject = new className(req, res,controllerName);
        myObject[methodName]();

    });

}

/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page. 
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
module.exports.add_get_path = (api,methodName) => {
    let apiController = ''

    let objectPath = '../api/'+api+'Api.js' 
	let className = require(objectPath);
	let titlePath = api; //api.toLowerCase();
    let controllerName = api+'Api';
    let path = '/api/'+ titlePath+'/'+methodName;
    
    Debugger.logconsole("GET :" + path);

    this.myapp.get(path,function (req, res) {
        let myObject = new className(req, res,controllerName);
        myObject[methodName]();

    });

}

module.exports.resource = (api) => {
    
    let apiController = ''

    let objectPath = '../api/'+api+'Api.js' 
	let className = require(objectPath);
	let titlePath = api; //api.toLowerCase();
    let controllerName = api+'Api';
    let path = '/api/'+ titlePath;
    Debugger.logconsole("<< resources >>");

    Debugger.logconsole("GET :" + path);
    this.myapp.get(path,function (req, res) {
        let methodName = 'index';
        let myObject = new className(req, res,controllerName);
        myObject[methodName]();

    }); 

    Debugger.logconsole("GET :" + path+'/latest');
    this.myapp.get(path+'/latest',function (req, res) {
        let methodName = 'latest';
        let myObject = new className(req, res,controllerName);
        myObject[methodName]();

    }); 

    Debugger.logconsole("GET :" + path+'/:replyid');
    this.myapp.get(path+'/:replyid',function (req, res) {
        let methodName = 'get';
        let id = req.params.replyid;
        let myObject = new className(req, res,controllerName);
        myObject[methodName](id);

    }); 

    Debugger.logconsole("POST :" + path+'/search');
    this.myapp.post(path+'/search',function (req, res) {
        let methodName = 'search';
        let myObject = new className(req, res,controllerName);
        myObject[methodName]();

    }); 

    Debugger.logconsole("POST :" + path);
    this.myapp.post(path,function (req, res) {
        let methodName = 'save';
        let myObject = new className(req, res,controllerName);
        myObject[methodName]();

    }); 

    Debugger.logconsole("PUT :" + path+'/:replyid');
    this.myapp.put(path+'/:replyid', function(req, res){
        let methodName = 'update';
        let id = req.params.replyid;
        let myObject = new className(req, res,controllerName);
        myObject[methodName](id);

    });

    Debugger.logconsole("DELETE :" + path+'/:replyid');
    this.myapp.delete(path+'/:replyid', function(req, res){
        let methodName = 'delete';
        let id = req.params.replyid;
        let myObject = new className(req, res,controllerName);
        myObject[methodName](id);

    });

}

module.exports.add_custom_path = (path,apiModule, methodName, method) => {
   let objectPath = '../api/'+apiModule+'Api.js' 
    let className = require(objectPath);
    

   this.myapp[method](path,function (req, res) {
        let myObject = new className(req, res,'pushPackages');
        myObject[methodName](req.params);
   });

}

module.exports.add_mulipart = (path,apiModule, methodName, method) => {
    console.log('adding multipart');
    var mutilpart = require('connect-multiparty');
    var uploader = require('express-fileuploader');
    var S3Strategy = require('express-fileuploader-s3');

    this.myapp.use(path, mutilpart());

    uploader.use(new S3Strategy({
        uploadPath: config.AWS_S3_UPLOADPATH,
        headers: {
            'x-amz-acl': 'public-read'
        },
        options: {
             key: config.AWS_KEY,
             secret: config.AWS_SECRET,
             bucket: config.AWS_S3_BUCKET
        }
    }));

    this.myapp[method](path, function(req, res, next) {
        uploader.upload('s3', req.files['images'], function(err, files) {
            if (err) {
                return next(err);
            }
            console.log(files);
            /*
            [
                {
                    "fieldName":"images",
                    "originalFilename":"icons8-notification-center-50.png",
                    "path":"/tmp/T1Ex9xO5uvURHgWk8AA6jbLn.png",
                    "headers":
                        {
                            "content-disposition":"form-data; 
                            name=\"images\"; 
                            filename=\"icons8-notification-center-50.png\"",
                            "content-type":"image/png"},
                            "size":576,
                            "name":"0cd1f460-d099-11e8-9f3d-1f748d0f9543.png",
                            "type":"image/png",
                            "url":"http://s3.amazonaws.com/dicegeneric/uploads/0cd1f460-d099-11e8-9f3d-1f748d0f9543.png"
                        }
            ]
            */
            res.send(JSON.stringify(files));
        });
    });

}
module.exports.startRoutes = (app) => {
    this.myapp = app; 
    
    //static pages for compliance
    this.add_static_path('/', 'Hello, there is nothing here!');
    
    this.add_post_path('authentication','forgotPassword');
    this.add_post_path('authentication','resetpass');
    this.add_post_path('authentication','register');
    this.add_post_path('authentication', 'login_no_client');
    this.add_post_path('notification','store_vapid');
    this.add_post_path('notification','send_notification_by_jwt');
    this.add_post_path('notification','send_notification');
    this.add_post_path('notification','push_sample');
    this.add_post_path('notification','my');
    this.add_post_path('notification','push_sample');
    this.add_get_path('profile','inbox');
    this.add_get_path('user','all');
    
    this.resource('profile');
    this.resource('user');
    this.resource('role');

    
    //create the routes for safari push notifcation (only for safari)
    this.add_custom_path ('/api/notification/inbox/:user_id','notification', 'get_inbox','get');
    //create the routes for safari push notifcation (only for safari)
    this.add_custom_path ('/:version/pushPackages/:websitePushID','notification', 'push_packages','get');
    //create the routes for safari push notifcation (only for safari)
    this.add_custom_path('/:version/pushPackages/:websitePushID','notification', 'push_packages','post');
    //register a new safari
    this.add_custom_path('/:version/devices/:deviceToken/registrations/:websitePushID','notification', 'register','post');
    //UNregister a new safari
    this.add_custom_path('/:version/devices/:deviceToken/registrations/:websitePushID','notification', 'unregister','delete');
    //register a new safari
    this.add_custom_path('/:version/devices/log','notification', 'log','post');
    //added api path for logging (safari)
    this.add_custom_path('/:version/log','notification', 'log','post');
    this.add_mulipart('/api/upload','', '', 'post');

}