const {SMTP_URL, USERNAME, PASSWORD} = process.env;
const nodemailer = require('nodemailer');

const FROM_EMAIL = process.env.FROM_EMAIL || 'no-reply@dice205.com';

const defaultEmailData = {from: FROM_EMAIL};
const template_directory = 'https://salto.com.ph/wp-content/themes/salto';
const company_logo = process.env.COMPANY_LOGO ||  'https://www.dice205.com/wp-content/themes/customtheme/assets/dice-logo.svg';
const footer = '<div class="social-links">\
	<a href="http://salto.com.ph/"><img class="inlineSocialLinks" src="' + template_directory + '/images/www.jpg"></a>\
	<a href="mailto:pilmico@aboitiz.com"><img class="inlineSocialLinks" src="' + template_directory + '/images/envelope.jpg"></a>\
	<a href="https://www.facebook.com/SALTO.OFFICIAL"><img class="inlineSocialLinks" src="' + template_directory + '/images/facebook.jpg"></a>\
	<a href="https://www.instagram.com/saltoph/"><img class="inlineSocialLinks" src="' + template_directory + '/images/instagram.jpg"></a>\
	<a href="https://www.youtube.com/channel/UCgrnl-T2Q9KOLQSLSpg5EqA"><img class="inlineSocialLinks" src="' + template_directory + '/images/youtube.jpg"></a>\
</div>';

const email_message = '\
<table align="center" id="mailTemplate" border="0" cellspacing="0" cellpadding="10">\
    \
    <tr class="headerTemplate">\
        <td align="center" colspan="6"><img src="' + company_logo + '" /></td>\
    </tr>\
\
    <tr>\
        <td colspan="12"> EMAIL_BODY \
         </td>\
    </tr>\
\
    <tr class="redTemplate footer-block">\
        <td colspan="12">\
\ '+footer+'\
        </td>\
    </tr>\
</table>\
';

const headers = "From: DICE205 <" + FROM_EMAIL +"> \
Reply-To: "+ FROM_EMAIL +" \
MIME-Version: 1.0 \
Content-Type: text/html; charset=ISO-8859-1 \
";

const startMessage = '<html>\
				<head>\
					<style type="text/css">\
						* {\
							font-family: Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;\
							font-size: 17px;\
							line-height: 27px;\
						}\
\
						img { max-width: 80%; }\
\
						#mailTemplate {\
							width: 600px;\
							border: 1px solid #000;\
						}\
\
						.uppercase { text-transform: uppercase; }\
\
						.headerTemplate {\
							background-color: #5C89D5;\
							padding:10px;\
						}\
\
						.darkTemplate {\
							background-color: #000;\
							color: #fff;\
						}\
\
						h1.uppercase { font-size: 40px; }\
\
						.inlineSocialLinks { display: inline-block; width: 25px; }\
						\
						.social-links {\
							display: inline-block;\
							margin-top: 10px;\
							float: left;\
						}\
\
						.link-block {\
							display: inline-block;\
							margin-left: 5px;\
							float: left;\
							margin-top: 10px;\
						}\
\
						.footer-block * {\
							color: #fff;\
							font-size: 10px;\
						}\
\
						.footer-link {\
							color: #fff;\
						}\
					</style>\
				</head>\
			<body>';

const endMessage = "</body></html>";
            
const sendEmail = (emailData, smtpUrl=SMTP_URL) => {
    
    let email_body = emailData.html;
    
    emailData.html = startMessage + email_message.replace('EMAIL_BODY',email_body) + endMessage;

    const completeEmailData = Object.assign(defaultEmailData, emailData);
    const transporter = nodemailer.createTransport({
        host: 'smtp.sendgrid.net',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'apikey', // generated ethereal user
            pass: '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' // generated ethereal password
        }
    });
    return transporter
        .sendMail(completeEmailData)
        .then(info => console.log(`Message sent: ${info.response}`))
        .catch(err => console.log(`Problem sending email: ${err}`));

}

module.exports = {sendEmail};