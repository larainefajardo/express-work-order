// import entire SDK
const AWS = require('aws-sdk');
// import AWS object without services
//const AWS = require('aws-sdk/global');
// import individual service

const config = require('../config');
const constants = require('../constants');
AWS.config.update({region: config.AWS_REGION, accessKeyId: config.AWS_KEY ,secretAccessKey: config.AWS_SECRET });
// Create S3 service object
s3 = new AWS.S3({apiVersion: config.AWS_API_VERSION});
module.exports.putObject = (filename) => {

}

module.exports.getObject = (filename) => {
    var params = {Bucket: config.AWS_S3_BUCKET, Key: filename, Expires: 10};
    var url = s3.getSignedUrl('getObject', params);
    console.log('The URL is', url) + '\n'; // expires in 60 seconds
}

module.exports.createBucket = () => {    
    let params = {
        Bucket: config.AWS_S3_BUCKET, 
        ACL: 'public-read',
        // CreateBucketConfiguration: {
        //  LocationConstraint: config.AWS_REGION
        // }
    };
    
    return new Promise(function(resolve, reject) {
        s3.createBucket(params, function(err, data) {
            if (err) {
               console.log(err, err.stack); // an error occurred
               reject(err);
            }else {
               console.log(data);           // successful response
               resolve(true);
            }    
            /*
            data = {
             Location: "http://examplebucket.s3.amazonaws.com/"
            }
            */
       });
    })
    
}