const mongoose = require('mongoose');
const validator = require('validator')
const jwt = require('jsonwebtoken');
const config = require('../config');
const AutoIncrement  = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;


// var permission = new Schema({Name: { type : String, index: true}});

var roleCopy = new Schema({
	role_id : { type : Number, index: true},
	permissions: [String],
	name : { type : String, index: true},
});

var password_history = new Schema({
	last_update : { type : Date, default: Date.now()},
	password	: { type:  String, index: true, require : true, minlength: 8 },	
});

var notifications = new Schema({
	sent_date 	: { type : Date, default: Date.now()},
	message		: { type:  String, require : true},	
	title		: { type:  String, require : true},	
	from		: {type: Number, index: true},
	read 		:	{ type:  Boolean, default: false},
});

var push_tokens = new Schema({
	type : { type:  String},
	details	: { type:  String, index: true},	
});

var UserSchema = new Schema({
	user_id: {type: Number, index: true},
	email: {
		type: String, 
		index: true, 
		minlength: 1, 
		unique: true, 
		trim: true,
		validate :{
			validator: validator.isEmail,
			message: '{VALUE} is not a valid email'
		}
	},
	original_email: {
		type: String, 
		index: true, 
		default: ''
	},
	password:		{ type:  String, index: true, require : true, minlength: 8 },
	webpush_tokens:	[push_tokens],
	full_name:		{ type:  String, index: true},
	first_name:		{ type:  String, index: true},
	last_name:		{ type:  String, index: true},

	gender:			{ type:  String, index: true},
	country:		{ type:  String, index: true},
	city:			{ type:  String, index: true},
	
	thirdparty_id :	{ type:  String, index: true,  default: null},
	
	//FB for facebook, GP for googleplus
	thirdparty_source :	{ type:  String, index: true,  default: null},
	
	confirmed :		{ type:  Boolean, index: true, default: false},
	contact_number:	{ type:  String, index: true},
	role: 			roleCopy,

	password_history: [password_history], 
	//audit related values
	created_by: 	{ type : Number, default: 0, index: true },
	created_at: 	{ type : Date, default: Date.now(), index: true },
	deleted_by: 	{ type : Number, default: 0, index: true },
	deleted_at: 	{ type : Date, default: null, index: true },
	resetPassLink: 	{ type : String, default: '',index: true},
	last_login: 	[{ type : Date, default: Date.now() }], 
	notification_list: 	[notifications],
});


UserSchema.plugin(AutoIncrement, {inc_field: 'user_id'});
module.exports = mongoose.model('User',UserSchema);