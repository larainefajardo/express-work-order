let mongoose = require('mongoose');
let AutoIncrement  = require('mongoose-sequence')(mongoose);
let Schema = mongoose.Schema;

var RoleSchema = new Schema({
	role_id : { type : Number, default: 0, index: true , unique: true},
	
	//details
	permissions: [],
	//audit related values
	name : { type : String, index: true},
	password_history_length : { type : Number, default: 6},
	
	max_password_age: { type : Number, default:180 },
	min_password_length: { type : Number, default:8 },

	has_special_characters : { type : Boolean, default:true },
	has_uppercase_characters : { type : Boolean, default:true },
	has_lowercase_characters : { type : Boolean, default:true },
	has_number : { type : Boolean, default:true },
	
	created_by: { type : Number, default: 0, index: true },
	created_at: { type : Date, default: Date.now(), index: true },
	deleted_by: { type : Number, default: 0, index: true },
	deleted_at: { type : Date, default: null, index: true }
});

RoleSchema.plugin(AutoIncrement, {inc_field: 'role_id'});
module.exports = mongoose.model('Role',RoleSchema);